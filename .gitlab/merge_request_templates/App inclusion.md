* [ ] The app complies with the [inclusion criteria](https://f-droid.org/wiki/page/Inclusion_Policy)
* [ ] The original app author has been notified (and supports the inclusion)
* [ ] All related fdroiddata and rfp issues have been referenced in this merge request
* [ ] I've considered adding parts of the [metadata to the app repo](https://f-droid.org/en/docs/All_About_Descriptions_Graphics_and_Screenshots/#in-the-apps-source-repository) in order to add screenshots and have a translatable description
* [ ] Builds with `fdroid build`

---------------------

